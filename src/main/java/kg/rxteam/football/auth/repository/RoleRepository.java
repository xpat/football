package kg.rxteam.football.auth.repository;



import kg.rxteam.football.auth.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository interface that extends {@link JpaRepository} for class {@link Role}.
 *
 */

public interface RoleRepository extends JpaRepository<Role, Long> {
    Role findByName(String name);
}
