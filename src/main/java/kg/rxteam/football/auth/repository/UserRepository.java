package kg.rxteam.football.auth.repository;


import kg.rxteam.football.auth.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository interface that extends {@link JpaRepository} for class {@link User}.
 *
 */

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String name);
}
