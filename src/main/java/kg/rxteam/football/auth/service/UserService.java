package kg.rxteam.football.auth.service;






import kg.rxteam.football.auth.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Service interface for class {@link User}.
 *
 */

public interface UserService {

    User register(User user, ArrayList<String> roles);

    List<User> getAll();

    User findByUsername(String username);

    User findById(Long id);

    void delete(Long id);
}
