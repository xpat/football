package kg.rxteam.football.auth.rest;



import kg.rxteam.football.auth.model.Role;
import kg.rxteam.football.auth.model.User;
import kg.rxteam.football.auth.security.jwt.JwtTokenProvider;
import kg.rxteam.football.auth.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * REST controller for authentication requests (login, logout, register, etc.)
 *
 * @author xpat
 */

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/api/")
public class AuthenticationRestController {

    private final AuthenticationManager authenticationManager;

    private final JwtTokenProvider jwtTokenProvider;

    private final UserService userService;

    @Autowired
    public AuthenticationRestController(AuthenticationManager authenticationManager, JwtTokenProvider jwtTokenProvider, UserService userService) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
        this.userService = userService;
    }

    @PostMapping("login")
    public ResponseEntity login(@RequestBody User user) {
        try {
            String username = user.getUsername();
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, user.getPassword()));
            user = userService.findByUsername(username);

            if (user == null) {
                throw new UsernameNotFoundException("User with username: " + username + " not found");
            }

            String token = jwtTokenProvider.createToken(username, user.getRoles());

            Map<Object, Object> response = new HashMap<>();
            response.put("id", user.getId());
            response.put("firstName", user.getFirstName());
            response.put("lastName", user.getLastName());
            response.put("email", username);
            response.put("token", token);
            response.put("role", user.isAdmin() ? "ADMIN" : "USER");

            return ResponseEntity.ok(response);
        } catch (AuthenticationException e) {
            throw new BadCredentialsException("Invalid username or password");
        }
    }

    @PostMapping("register")
    public ResponseEntity register(@RequestBody User user) {

        User existingUser = userService.findByUsername(user.getUsername());
        if (existingUser != null) {
            throw new BadCredentialsException("Пользователь с таким логином существует!");
        }

        ArrayList<String> roles = new ArrayList<>();
        roles.add(Role.ROLE_USER);
        user = userService.register(user,roles);

        return getResponseEntity(user);
    }

    private ResponseEntity getResponseEntity(User user) {
        String token = jwtTokenProvider.createToken(user.getUsername(), user.getRoles());
//
        Map<Object, Object> response = new HashMap<>();
        response.put("username", user.getUsername());
        response.put("token", token);

        return ResponseEntity.ok(response);
    }
}
