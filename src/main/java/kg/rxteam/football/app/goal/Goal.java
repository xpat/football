package kg.rxteam.football.app.goal;

import kg.rxteam.football.app.match.Match;
import kg.rxteam.football.app.player.Player;
import kg.rxteam.football.app.team.Team;
import kg.rxteam.football.auth.model.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Entity
@Table(name = "goals")
@Data
@EqualsAndHashCode(callSuper = false)
public class Goal extends BaseEntity {

    @Column(name = "goal_time")
    private String time;

    @ManyToOne(targetEntity = Match.class)
    @JoinColumn(nullable = false, referencedColumnName = "id")
    private Match match;

    @ManyToOne(targetEntity = Team.class)
    @JoinColumn(nullable = false, referencedColumnName = "id")
    private Team teamFrom;

    @ManyToOne(targetEntity = Team.class)
    @JoinColumn(nullable = false, referencedColumnName = "id")
    private Team teamTo;

    @ManyToOne(targetEntity = Player.class)
    @JoinColumn(nullable = false, referencedColumnName = "id")
    private Player author;

    @ManyToOne(targetEntity = Player.class)
    @JoinColumn(nullable = false, referencedColumnName = "id")
    private Player assistant;

    @Column(name = "auto_goal")
    private Boolean autoGoal = false;

}
