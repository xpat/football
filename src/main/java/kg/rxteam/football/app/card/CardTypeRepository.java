package kg.rxteam.football.app.card;

import kg.rxteam.football.app.card.CardType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CardTypeRepository extends JpaRepository<CardType,Long> {
}
