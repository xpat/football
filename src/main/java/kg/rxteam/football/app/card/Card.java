package kg.rxteam.football.app.card;


import kg.rxteam.football.app.match.Match;
import kg.rxteam.football.app.player.Player;
import kg.rxteam.football.auth.model.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;


@Entity
@Table(name = "cards")
@Data
@EqualsAndHashCode(callSuper = false)
public class Card extends BaseEntity {

    @Column(name = "card_time")
    private String time;

    @ManyToOne(targetEntity = Player.class)
    @JoinColumn(nullable = false, referencedColumnName = "id")
    private Player player;

    @ManyToOne(targetEntity = CardType.class)
    @JoinColumn(referencedColumnName = "id", nullable = false)
    private CardType type;

    @ManyToOne(targetEntity = Match.class)
    @JoinColumn(nullable = false, referencedColumnName = "id")
    private Match match;
}
