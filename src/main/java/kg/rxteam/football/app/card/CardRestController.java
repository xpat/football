package kg.rxteam.football.app.card;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/api/admin/card")
public class CardRestController {

    @RequestMapping(value = "test")
    public List<String> test(){
        List<String> list = new ArrayList<>();
        list.add("test 1");
        list.add("test 2");
        return list;
    }

}
