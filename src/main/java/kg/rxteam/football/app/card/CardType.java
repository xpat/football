package kg.rxteam.football.app.card;

import kg.rxteam.football.auth.model.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name = "card_types")
@Data
@EqualsAndHashCode(callSuper = false)

public class CardType extends BaseEntity {

    @Column(name = "name")
    private String name;
}
