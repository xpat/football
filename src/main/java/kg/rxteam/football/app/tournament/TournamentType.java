package kg.rxteam.football.app.tournament;

import kg.rxteam.football.auth.model.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "tournament_types")
@Data
@EqualsAndHashCode(callSuper = false)
public class TournamentType extends BaseEntity {

    @Column
    private String name;

}
