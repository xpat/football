package kg.rxteam.football.app.tournament;

import kg.rxteam.football.app.match.Match;
import kg.rxteam.football.app.team.Team;
import kg.rxteam.football.auth.model.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "tournaments")
@Data
@EqualsAndHashCode(callSuper = false)
public class Tournament extends BaseEntity {

    public Tournament() {
        this.teams = new ArrayList<>();
        this.matches = new ArrayList<>();
    }

    @Column
    private String name;

    @ManyToOne(targetEntity = TournamentType.class)
    @JoinColumn(nullable = false)
    private TournamentType type;

    @ManyToMany(targetEntity = Team.class, fetch = FetchType.LAZY)
    @JoinTable(name = "tournament_teams")
    private List<Team> teams;

    @OneToMany(targetEntity = Match.class, fetch = FetchType.LAZY,mappedBy = "tournament")
    private List<Match> matches;

}
