package kg.rxteam.football.app.tournament;

import kg.rxteam.football.app.tournament_table.TournamentTableItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TournamentRepository extends JpaRepository<Tournament,Long> {

    @Query(value = "SELECT t.id,name,\n" +
            "       ( SELECT count(id) FROM matches where guest_team_id = t.id or home_team_id = t.id ) as games,\n" +
            "        (SELECT count(id) FROM matches where winner_id = t.id and (guest_team_id = t.id or home_team_id = t.id) ) as wins,\n" +
            "        (SELECT count(id) FROM matches where winner_id not in (t.id,0) and (guest_team_id = t.id or home_team_id = t.id)) as loss,\n" +
            "        (SELECT count(id) FROM matches where winner_id = 0 and (guest_team_id = t.id or home_team_id = t.id)) as draw,\n" +
            "        (SELECT sum( CASE winner_id WHEN t.id THEN 3 WHEN 0 THEN 1 else 0 END) FROM matches where (guest_team_id = t.id or home_team_id = t.id)) as points\n" +
            "FROM teams t\n" +
            "left join tournament_teams tt on t.id = tt.teams_id\n" +
            "where tt.tournament_id = ?\n" +
            "ORDER BY points DESC,loss", nativeQuery = true)
    public List<Object[]> getItems(Integer tournamentId);
}
