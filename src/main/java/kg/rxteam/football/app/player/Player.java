package kg.rxteam.football.app.player;


import kg.rxteam.football.app.team.Team;
import kg.rxteam.football.auth.model.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "players")
@Data
@EqualsAndHashCode(callSuper = false)
public class Player extends BaseEntity {

    @ManyToOne
    @JoinColumn(nullable = false)
    private Team team;

    @Column
    private String firstName;

    @Column
    private String lastName;

    @Column
    private String fatherName;

    @Column
    private Date birthDate;

    @Column
    private Integer number;

    @Column
    private String image;


}
