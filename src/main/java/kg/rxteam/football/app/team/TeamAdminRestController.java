package kg.rxteam.football.app.team;

import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/api/admin/team")
public class TeamAdminRestController {

    private TeamRepository repository;

    @Autowired
    public TeamAdminRestController(TeamRepository repository) {
        this.repository = repository;
    }

    @GetMapping(value = "")
    public List<Team> list(){
        return repository.findAll();
    }

    @PostMapping(value = "")
    public Team create(@Valid @RequestBody Team team) {
        team = this.repository.save(team);
        return team;
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity edit(@PathVariable("id") Long id, @Valid @RequestBody Team team) throws NotFoundException {
        if (!repository.findById(id).isPresent()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        team.setId(id);
        team = this.repository.save(team);
        return ResponseEntity.ok(team);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity delete(@PathVariable("id") Long id) throws NotFoundException {
        if (repository.findById(id).isPresent()) {
            this.repository.delete(repository.findById(id).get());
            return new ResponseEntity(HttpStatus.OK);
        }

        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

}
