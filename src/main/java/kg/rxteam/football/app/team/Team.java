package kg.rxteam.football.app.team;

import kg.rxteam.football.app.player.Player;
import kg.rxteam.football.auth.model.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "teams")
@Data
@EqualsAndHashCode(callSuper = false)
public class Team extends BaseEntity {

    public Team() {
        this.players = new ArrayList<>();
    }

    @Column
    @NotEmpty(message = "Значение не может быть пустым")
    private String name;

    @OneToMany(mappedBy = "team")
    private List<Player> players;

    @Column
    @NotEmpty(message = "Значение не может быть пустым")
    private String contacts;

    @Column
    private String image;
}
