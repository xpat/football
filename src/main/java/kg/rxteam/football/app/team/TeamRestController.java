package kg.rxteam.football.app.team;

import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/api/team")
public class TeamRestController {

    private TeamRepository repository;

    @Autowired
    public TeamRestController(TeamRepository repository) {
        this.repository = repository;
    }

    @GetMapping(value = "")
    public List<Team> list(){
        return repository.findAll();
    }

}
