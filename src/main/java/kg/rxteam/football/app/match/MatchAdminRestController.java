package kg.rxteam.football.app.match;

import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/api/admin/match")
public class MatchAdminRestController {

    private MatchRepository repository;

    @Autowired
    public MatchAdminRestController(MatchRepository repository) {
        this.repository = repository;
    }

    @GetMapping(value = "")
    public List<Match> list(){
        return repository.findAll();
    }

    @PostMapping(value = "")
    public Match create(@Valid @RequestBody Match entity) {
        entity = this.repository.save(entity);
        return entity;
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity edit(@PathVariable("id") Long id, @Valid @RequestBody Match entity) throws NotFoundException {
        if (!repository.findById(id).isPresent()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        entity = this.repository.save(entity);
        return ResponseEntity.ok(entity);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity delete(@PathVariable("id") Long id) throws NotFoundException {
        if (repository.findById(id).isPresent()) {
            this.repository.delete(repository.findById(id).get());
            return new ResponseEntity(HttpStatus.OK);
        }

        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

}
