package kg.rxteam.football.app.match;

import kg.rxteam.football.app.card.Card;
import kg.rxteam.football.app.goal.Goal;
import kg.rxteam.football.app.tournament.Tournament;
import kg.rxteam.football.app.team.Team;
import kg.rxteam.football.auth.model.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "matches")
@Data
@EqualsAndHashCode(callSuper = false)
public class Match extends BaseEntity {

    public Match(Tournament tournament, Team homeTeam, Team guestTeam) {
        this.tournament = tournament;
        this.homeTeam = homeTeam;
        this.guestTeam = guestTeam;
        this.goals = new ArrayList<>();
        this.yellowCards = new ArrayList<>();
        this.redCards = new ArrayList<>();
    }

    @ManyToOne(targetEntity = Tournament.class)
    @JoinColumn(nullable = false, referencedColumnName = "id")
    private Tournament tournament;

    @ManyToOne(targetEntity = Team.class)
    @JoinColumn(nullable = false, referencedColumnName = "id")
    private Team homeTeam;

    @ManyToOne(targetEntity = Team.class)
    @JoinColumn(nullable = false, referencedColumnName = "id")
    private Team guestTeam;

    @ManyToOne(targetEntity = Team.class)
    @JoinColumn(nullable = false, referencedColumnName = "id")
    private Team winner;

    @OneToMany(targetEntity = Goal.class, mappedBy = "match")
    private List<Goal> goals;

    @OneToMany(targetEntity = Card.class, mappedBy = "match")
    private List<Card> yellowCards;

    @OneToMany(targetEntity = Card.class, mappedBy = "match")
    private List<Card> redCards;

    @Column(name = "match_date")
    private Date date;

}
