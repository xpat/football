package kg.rxteam.football.app.tournament_table;

import kg.rxteam.football.app.team.Team;
import lombok.Data;

import java.math.BigDecimal;
import java.math.BigInteger;

@Data
public class TournamentTableItem {

    Integer number;

    BigInteger teamId;

    String team;

    BigInteger matches;

    BigInteger wins;

    BigInteger loss;

    BigInteger draw;

    BigDecimal points;

}
