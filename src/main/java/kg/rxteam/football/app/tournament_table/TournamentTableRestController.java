package kg.rxteam.football.app.tournament_table;

import kg.rxteam.football.app.tournament.TournamentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/api/tournament-table")
public class TournamentTableRestController {

    private TournamentRepository repository;

    @Autowired
    public TournamentTableRestController(TournamentRepository repository) {
        this.repository = repository;
    }

    @GetMapping(value = "/{tournamentId}")
    public List<TournamentTableItem> list(@PathVariable Integer tournamentId) {
        List<Object[]> items = repository.getItems(tournamentId);

        ArrayList<TournamentTableItem> result = new ArrayList<>();

        Integer i = 0;
        for (Object[] item : items) {

            i++;
            TournamentTableItem tableItem = new TournamentTableItem();

            tableItem.number = i;
            tableItem.teamId = (BigInteger) item[0];
            tableItem.team = (String) item[1];
            tableItem.matches = (BigInteger) item[2];
            tableItem.wins = (BigInteger) item[3];
            tableItem.loss = (BigInteger) item[4];
            tableItem.draw = (BigInteger) item[5];
            tableItem.points = (BigDecimal) item[6];
            result.add(tableItem);
        }

        return result;
    }


}
